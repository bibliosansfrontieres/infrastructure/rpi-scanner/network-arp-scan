const macAddressRegex =
  /^[0-9a-f]{1,2}([\.:-])(?:[0-9a-f]{1,2}\1){4}[0-9a-f]{1,2}$/;

export function isMacAddress(string: string): boolean {
  return macAddressRegex.test(string);
}
