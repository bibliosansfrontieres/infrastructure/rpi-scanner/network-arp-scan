export interface IDevice {
  ip: string;
  macAddress: string;
  name: string | null;
  hostname?: string;
  isAlive: boolean;
}
