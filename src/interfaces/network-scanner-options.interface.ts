export interface INetworkScannerOptions {
  arpScanPath?: string;
  logging?: boolean;
  debugging?: boolean;
  interface?: string;
  pingingDevices?: boolean;
  pingTimeout?: number;
  pingThreads?: number;
  gettingHostnames?: boolean;
  gettingHostnamesThreads?: number;
  gettingHostnamesTimeout?: number;
}
