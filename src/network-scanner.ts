import { INetworkScannerOptions } from './interfaces/network-scanner-options.interface';
import { exec } from 'child_process';
import { isIp } from './utils/is-ip.util';
import { isMacAddress } from './utils/is-mac-address.util';
import { IDevice } from './interfaces/device.interface';

export class NetworkScanner {
  public logging: boolean;
  public debugging: boolean;
  public interface: string | undefined;
  public arpScanPath: string = '';
  public pingingDevices: boolean;
  public pingTimeout: number;
  public pingThreads: number;
  public gettingHostnames: boolean;
  public gettingHostnamesTimeout: number;
  public gettingHostnamesThreads: number;

  constructor(options?: INetworkScannerOptions) {
    this.logging = options?.logging ?? false;
    this.debugging = options?.debugging ?? false;
    this.interface = options?.interface;
    if (options?.arpScanPath) {
      if (!options.arpScanPath.endsWith('/')) {
        this.arpScanPath = options.arpScanPath + '/';
      } else {
        this.arpScanPath = options.arpScanPath;
      }
    }
    this.pingingDevices = options?.pingingDevices ?? false;
    this.pingTimeout = options?.pingTimeout ?? 5000;
    this.pingThreads = options?.pingThreads ?? 1;
    this.gettingHostnames = options?.gettingHostnames ?? false;
    this.gettingHostnamesThreads = options?.gettingHostnamesThreads ?? 1;
    this.gettingHostnamesTimeout = options?.gettingHostnamesTimeout ?? 5000;
  }

  debug(...args: any[]) {
    if (this.debugging) {
      console.log(...args);
    }
  }

  log(...args: any[]) {
    if (this.logging) {
      console.log(...args);
    }
  }

  error(...args: any[]) {
    if (this.logging) {
      console.error(...args);
    }
  }

  getArpScanOptions() {
    const arpScanOptions: string[] = [];

    if (this.interface) {
      arpScanOptions.push(`-I ${this.interface}`);
    }

    arpScanOptions.push('-l');

    return arpScanOptions;
  }

  async useArpScan() {
    const arpScanOptions = this.getArpScanOptions();
    try {
      const arpScanResult = await new Promise<string>((resolve, reject) => {
        exec(
          `${this.arpScanPath}arp-scan ${arpScanOptions}`,
          (error, stdout, stderr) => {
            if (error) {
              this.error(error);
              reject(error);
            }

            if (stderr) {
              this.error(stderr);
              reject(stderr);
            }

            resolve(stdout);
          },
        );
      });
      return arpScanResult;
    } catch (e) {
      this.error(e);
      return undefined;
    }
  }

  parseArpScanResult(arpScanResult: string) {
    const foundDevices: IDevice[] = [];

    const lines = arpScanResult.split('\n');
    for (let i = 0; i < lines.length; i++) {
      const line = lines[i];
      const parts = line.split('\t');

      const ip = parts[0];
      const macAddress = parts[1];
      let name = parts[2] || null;

      if (!isIp(ip) || !isMacAddress(macAddress)) {
        continue;
      }

      if (name === '(Unknown: locally administered)') {
        name = null;
      }

      foundDevices.push({
        ip,
        macAddress,
        name,
        isAlive: false,
      });
    }

    return foundDevices;
  }

  async getHostnames(devices: IDevice[]) {
    const processedDevices: IDevice[] = [];
    const getHostnamesThreads: Promise<void>[] = [];

    for (
      let i = 0;
      i < this.gettingHostnamesThreads && i < devices.length;
      i++
    ) {
      getHostnamesThreads.push(
        this.getHostnamesThread(devices, processedDevices),
      );
    }

    await Promise.all(getHostnamesThreads);

    return processedDevices;
  }

  async getHostnamesThread(devices: IDevice[], processedDevices: IDevice[]) {
    while (devices.length > 0) {
      const device = devices.shift();
      if (!device) continue;
      const processedDevice = await this.getHostname(device);
      processedDevices.push(processedDevice);
    }
  }

  async getHostname(device: IDevice) {
    const hostname = await this.useNslookup(device);
    device.hostname = hostname;
    return device;
  }

  async useNslookup(device: IDevice): Promise<string | undefined> {
    try {
      const nslookupResult = await new Promise<string>((resolve, reject) => {
        exec(`nslookup ${device.ip}`, (error, stdout, stderr) => {
          if (error) {
            this.error(error);
            reject(error);
          }

          if (stderr) {
            this.error(stderr);
            reject(stderr);
          }

          resolve(stdout);
        });
        setTimeout(reject, this.gettingHostnamesTimeout);
      });

      if (!nslookupResult.includes('name = ')) return undefined;

      const parseNslookupResult = nslookupResult.split('name = ');
      const fullHostname = parseNslookupResult[1];
      if (!fullHostname) return undefined;

      const hostname = fullHostname.split('.')[0];
      return hostname;
    } catch (e) {
      this.error(e);
      return undefined;
    }
  }

  async pingDevices(devices: IDevice[]) {
    const pingedDevices: IDevice[] = [];
    const pingThreads: Promise<void>[] = [];

    for (let i = 0; i < this.pingThreads && i < devices.length; i++) {
      pingThreads.push(this.pingDevicesThread(devices, pingedDevices));
    }

    await Promise.all(pingThreads);

    return pingedDevices;
  }

  async pingDevicesThread(devices: IDevice[], pingedDevices: IDevice[]) {
    while (devices.length > 0) {
      const device = devices.shift();
      if (!device) continue;
      const pingedDevice = await this.pingDevice(device);
      pingedDevices.push(pingedDevice);
    }
  }

  async usePing(ip: string): Promise<boolean> {
    try {
      const pingResult = await new Promise<boolean>((resolve, reject) => {
        const pingCommand = `ping -c 1 -W ${Math.round(
          this.pingTimeout / 1000,
        )} ${ip}`;

        exec(pingCommand, (error) => {
          if (error) reject(error);
          resolve(true);
        });

        // Set a timeout for the ping command
        setTimeout(() => {
          resolve(false); // Resolve with false if the ping takes longer than the specified timeout
        }, this.pingTimeout);
      });
      return pingResult;
    } catch (e) {
      this.error(e);
      return false;
    }
  }

  async pingDevice(device: IDevice): Promise<IDevice> {
    const isAlive = await this.usePing(device.ip);
    return {
      ...device,
      isAlive,
    };
  }

  async scanNetwork() {
    this.debug('[network-arp-scan] - Scanning network...');

    const scanResult = await this.useArpScan();

    if (!scanResult) {
      return [];
    }

    let devices = this.parseArpScanResult(scanResult);

    this.debug(
      `[network-arp-scan] - Network scanned ! Found ${devices.length} devices.`,
    );

    if (this.pingingDevices) {
      this.debug(`[network-arp-scan] - Pinging devices...`);

      devices = await this.pingDevices(devices);

      this.debug(`[network-arp-scan] - Devices pinged !`);
    }

    if (this.gettingHostnames) {
      this.debug(`[network-arp-scan] - Retriving devices hostnames...`);

      devices = await this.getHostnames(devices);

      this.debug(`[network-arp-scan] - Hostnames retrieved !`);
    }

    return devices;
  }
}
