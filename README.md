# Network ARP Scan

This node package is used to scan network using `arp-scan`.

Your node process needs access to `arp-scan`, the easiest way to do it without sudo access is with Docker.

## Example

You have an example of a function that scan network and return the devices found :

```
import { NetworkScanner } from '@bibliosansfrontieres/network-arp-scan';

async function main() {
    const options = {
        // You can use all options from INetworkScannerOptions
    };
    const networkScanner = new NetworkScanner(options);
    const devicesFound = await networkScanner.scanNetwork();
    console.log(devicesFound);
    return devicesFound;
}

main();
```
