import { NetworkScanner } from '../src/network-scanner';

describe('NetworkScanner', () => {
  describe('constructor', () => {
    test('should initialize with default options if no options provided', () => {
      const scanner = new NetworkScanner();
      expect(scanner.logging).toBe(false);
      expect(scanner.debugging).toBe(false);
      expect(scanner.interface).toBeUndefined();
      expect(scanner.arpScanPath).toBe('');
      expect(scanner.pingingDevices).toBe(false);
      expect(scanner.pingTimeout).toBe(5000);
      expect(scanner.pingThreads).toBe(1);
      expect(scanner.gettingHostnames).toBe(false);
      expect(scanner.gettingHostnamesTimeout).toBe(5000);
      expect(scanner.gettingHostnamesThreads).toBe(1);
    });

    test('should initialize with provided options', () => {
      const options = {
        logging: true,
        debugging: true,
        interface: 'eth0',
        arpScanPath: '/usr/bin/',
        pingingDevices: true,
        pingTimeout: 3000,
        pingThreads: 2,
        gettingHostnames: true,
        gettingHostnamesTimeout: 2000,
        gettingHostnamesThreads: 3,
      };
      const scanner = new NetworkScanner(options);
      expect(scanner.logging).toBe(true);
      expect(scanner.debugging).toBe(true);
      expect(scanner.interface).toBe('eth0');
      expect(scanner.arpScanPath).toBe('/usr/bin/');
      expect(scanner.pingingDevices).toBe(true);
      expect(scanner.pingTimeout).toBe(3000);
      expect(scanner.pingThreads).toBe(2);
      expect(scanner.gettingHostnames).toBe(true);
      expect(scanner.gettingHostnamesTimeout).toBe(2000);
      expect(scanner.gettingHostnamesThreads).toBe(3);
    });
  });

  describe('useArpScan', () => {
    test('should return ARP scan result with one device', async () => {
      const scanner = new NetworkScanner();
      const arpScanResult = '192.168.1.1\t00:11:22:33:44:55\t';
      jest.spyOn(scanner as any, 'useArpScan').mockResolvedValue(arpScanResult);

      const result = await scanner.scanNetwork();

      expect(result).toEqual([
        {
          ip: '192.168.1.1',
          macAddress: '00:11:22:33:44:55',
          name: null,
          isAlive: false,
        },
      ]);
    });
  });

  describe('parseArpScanResult', () => {
    test('should parse ARP scan result with single device', () => {
      const scanner = new NetworkScanner();
      const arpScanResult = '192.168.1.1\t00:11:22:33:44:55\t';

      const result = scanner.parseArpScanResult(arpScanResult);

      expect(result).toEqual([
        {
          ip: '192.168.1.1',
          macAddress: '00:11:22:33:44:55',
          name: null,
          isAlive: false,
        },
      ]);
    });

    test('should parse ARP scan result with invalid entries', () => {
      const scanner = new NetworkScanner();
      const arpScanResult =
        'Invalid Line\n192.168.1.1\t00:11:22:33:44:55\t\nInvalid Line';

      const result = scanner.parseArpScanResult(arpScanResult);

      expect(result).toEqual([
        {
          ip: '192.168.1.1',
          macAddress: '00:11:22:33:44:55',
          name: null,
          isAlive: false,
        },
      ]);
    });
  });
  // Write more tests for other methods and functionalities of NetworkScanner class
});
